local event_listener = require("__event-listener__/branch-2/stable-version")
local modules = {}
modules.rocket_aggression = require("rocket_aggression/control")

event_listener.add_events(modules)
