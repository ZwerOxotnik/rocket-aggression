--[[
Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
Author: ZwerOxotnik
Version: 0.9.0 (2019.06.23)

You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/rocket-aggression
Mod portal: https://mods.factorio.com/mod/rocket-aggression
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=72402

]]--

local mod = {}
mod.version = "0.9.0"

local function on_rocket_launched(event)
	local enemy_expansion = game.map_settings.enemy_expansion
	if enemy_expansion.enabled == 1 then
		if enemy_expansion.max_expansion_distance < 25 then
			-- Distance in chunks from the furthest base around.
			-- This prevents expansions from reaching too far into the
			-- player's territory
			enemy_expansion.max_expansion_distance = enemy_expansion.max_expansion_distance + 1
		end

		-- enemy_expansion.friendly_base_influence_radius
		-- enemy_expansion.enemy_building_influence_radius

		-- Size of the group that goes to build new base (in game this is multiplied by the
		-- evolution factor).
		enemy_expansion.settler_group_min_size = enemy_expansion.settler_group_min_size + 1
		enemy_expansion.settler_group_max_size = enemy_expansion.settler_group_max_size + 1

		-- Ticks to expand to a single
		-- position for a base is used.
		--
		-- cooldown is calculated as follows:
		--   cooldown = lerp(max_expansion_cooldown, min_expansion_cooldown, -e^2 + 2 * e),
		-- where lerp is the linear interpolation function, and e is the current evolution factor.
		if enemy_expansion.min_expansion_cooldown > 9000 then
			enemy_expansion.min_expansion_cooldown = enemy_expansion.min_expansion_cooldown - 3000
		end
		if enemy_expansion.min_expansion_cooldown > 9000 then
			enemy_expansion.max_expansion_cooldown = enemy_expansion.max_expansion_cooldown - 3000
		end
	end

	local unit_group = game.map_settings.unit_group
	-- pollution triggered group waiting time is a random time between min and max gathering time
	-- unit_group.min_group_gathering_time = 3600,
	-- unit_group.max_group_gathering_time = 10 * 360

	-- after the gathering is finished the group can still wait for late members,
	-- but it doesn't accept new ones anymore
	-- unit_group.max_wait_time_for_late_members = 2 * 3600

	-- limits for group radius (calculated by number of numbers)
	-- unit_group.max_group_radius = 30.0
	-- unit_group.min_group_radius = 5.0

	-- when a member falls behind the group he can speedup up till this much of his regular speed
	-- unit_group.max_member_speedup_when_behind = 1.4

	-- When a member gets ahead of its group, it will slow down to at most this factor of its speed
	-- unit_group.max_member_slowdown_when_ahead = 0.6

	-- When members of a group are behind, the entire group will slow down to at most this factor of its max speed
	-- unit_group.max_group_slowdown_factor = 0.3

	-- If a member falls behind more than this times the group radius, the group will slow down to max_group_slowdown_factor
	-- unit_group.max_group_member_fallback_factor = 3

	-- If a member falls behind more than this time the group radius, it will be removed from the group.
	-- unit_group.member_disown_distance = 10
	-- unit_group.tick_tolerance_when_member_arrives = 60

	-- Maximum number of automatically created unit groups gathering for attack at any time.
	unit_group.max_gathering_unit_groups = unit_group.max_gathering_unit_groups + 1

	-- Maximum size of an attack unit group. This only affects automatically-created unit groups; manual groups
	-- created through the API are unaffected.
	-- unit_group.max_unit_group_size = 200
end

mod.events = {
	[defines.events.on_rocket_launched] = on_rocket_launched
}

return mod
